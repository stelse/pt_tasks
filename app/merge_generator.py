from typing import Iterator, Generator, List


def get_next_elements(*input_iterables: Iterator[int]) -> List[int]:
    """
    A function that returns a list of the following iterator values

    :param input_iterables: iterators whose values are sorted
     from largest to smallest
    :return: list of next elements
    """
    elements = []
    for iterable in input_iterables:
        try:
            next_it = next(iterable)
        except StopIteration:
            continue

        if type(next_it) != int:
            raise TypeError('Iterables must contain integer values')

        elements.append(next_it)
    return elements


def merge(*input_iterables: Iterator[int]) -> Generator[int, None, None]:
    """
    A generator which accepts as arguments an arbitrary number of iterables,
    each of which generates sorted numbers, not necessarily one after another

    :param input_iterables: iterators whose values are sorted
     from largest to smallest
    :return: Generator
    """
    if not all(
        [isinstance(iterable, Iterator) for iterable in input_iterables]
    ):
        raise TypeError('Some count input objects not iterable')

    next_elements = get_next_elements(*input_iterables)

    while True:
        min_elem = min(next_elements)
        next_elements.remove(min_elem)
        yield min_elem

        if next_elements and min(next_elements) == min_elem:
            continue

        new_elements = get_next_elements(*input_iterables)
        if all(
            [element >= max(next_elements) for element in new_elements]
        ):
            next_elements.extend(new_elements)
        else:
            raise ValueError('Input iterators not sorted')

        if not next_elements:
            break
