import re


TIME_PATTERN = re.compile(
    r'^(?P<time_value>-?\d+(?:[.,]\d{1,2})?)(?P<unit_of_time>[smhd]?)$'
)
MAP_UNIT_OF_TIME = {
    's': 1,
    'm': 60,
    'h': 60 * 60,
    'd': 24 * 60 * 60
}


def get_seconds_from_str_timedelta(timedelta_string: str) -> int:
    """
    A function that converts a timedelta in string format to integer format
    with mathematical rounding

    :param timedelta_string: timedelta in string format
    :return: integer time format in seconds with mathematical rounding
    """
    if type(timedelta_string) != str:
        raise TypeError('Timedelta must be str')

    if not timedelta_string:
        raise ValueError('Input timedelta dont must be empty')

    if timedelta_string in MAP_UNIT_OF_TIME:
        return MAP_UNIT_OF_TIME[timedelta_string]

    parsed_time = TIME_PATTERN.search(timedelta_string)
    if parsed_time is None:
        raise ValueError('Incorrect timedelta format')

    time_value = float(parsed_time['time_value'].replace(',', '.'))

    if parsed_time['unit_of_time']:
        time_value = time_value * MAP_UNIT_OF_TIME[parsed_time['unit_of_time']]

    return int(time_value + (0.55 if time_value > 0 else -0.55))
