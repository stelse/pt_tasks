from pathlib import Path
from typing import Generator, Tuple
from itertools import islice


def read_next_line(path_object: Path, start_position: int) -> Tuple[str, int]:
    """
    A function that reads a line from a file starting at a specified position
    If the file is over, then reading starts from line 0

    :param path_object: path object that represents the rpath to file
    :param start_position: line number to read from file
    :return: read line and next line number
    """
    end_position = start_position + 1
    with path_object.open() as f:
        try:
            line = next(islice(f, start_position, end_position))
            return line, end_position
        except StopIteration:
            pass

    if start_position > 0:
        line, end_position = read_next_line(path_object, 0)
    else:
        line = str()
        end_position = 0

    return line, end_position


def file_generator(*input_file_paths: str) -> Generator[str, None, None]:
    """
    A eternal generator, which outputs files in
    a multiplexed file-by-file line-by-line order

    :param input_file_paths: paths to files in string format
    :return: Generator
    """
    line_positions = dict()
    for file_path in input_file_paths:
        if type(file_path) is not str:
            raise TypeError('File path must be str')

        path_object = Path(file_path)
        line_positions[path_object] = 0

    while True:
        for path_object, start_position in line_positions.items():
            line, end_position = read_next_line(path_object, start_position)
            line_positions[path_object] = end_position
            yield line.strip()
