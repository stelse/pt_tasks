import pytest
from app.time_func import get_seconds_from_str_timedelta


@pytest.mark.parametrize(
    "input_test_data,expected_seconds",
    [
        ('30', 30),
        ('30s', 30),
        ('s', 1),
        ('60.5m', 3630),
        ('10m', 600),
        ('5h', 5 * 60 * 60),
        ('2d', 2 * 24 * 60 * 60),
        ('m', 60),
        ('h', 60 * 60),
        ('d', 24 * 60 * 60),
        ('0', 0),
        ('-0', 0),
        ('60,5m', 3630),
        ('-10s', -10),
        ('1.9', 2),
        ('2.5', 3),
        ('-0.9', -1),
        ('0.1', 0)
    ]
)
def test_parse_timedelta(input_test_data, expected_seconds):
    result_seconds = get_seconds_from_str_timedelta(input_test_data)
    assert result_seconds == expected_seconds


@pytest.mark.parametrize(
    'input_test_data,expected_error',
    [
        pytest.param(
            '60seconds',
            {
                'expected_exception': ValueError,
                'match': 'Incorrect timedelta format',
            },
            id='incorrect_unit_of_time_60seconds'
        ),
        pytest.param(
            '',
            {
                'expected_exception': ValueError,
                'match': 'Input timedelta dont must be empty',
            },
            id='empty_string'
        ),
        pytest.param(
            '1y',
            {
                'expected_exception': ValueError,
                'match': 'Incorrect timedelta format',
            },
            id='incorrect_unit_of_time_y'
        ),
        pytest.param(
            '10.200',
            {
                'expected_exception': ValueError,
                'match': 'Incorrect timedelta format',
            },
            id='incorrect_format_timedelta_10.200'
        ),
        pytest.param(
            '-s',
            {
                'expected_exception': ValueError,
                'match': 'Incorrect timedelta format',
            },
            id='incorrect_format_timedelta_-s'
        ),
        pytest.param(
            None,
            {
                'expected_exception': TypeError,
                'match': 'Timedelta must be str',
            },
            id='none_type_input_timedelta'
        )
    ]
)
def test_parse_timedelta_exceptions(input_test_data, expected_error):
    with pytest.raises(**expected_error):
        get_seconds_from_str_timedelta(input_test_data)
