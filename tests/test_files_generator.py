from pathlib import Path

import pytest

from app.files_generator import read_next_line, file_generator


@pytest.mark.parametrize(
    "input_file_path,start_position,expected_line,expected_end_position",
    [
        (
            Path('./tests/test_data/file2'),
            2,
            'C\n',
            3
        ),
        (
            Path('./tests/test_data/file4'),
            0,
            '',
            0
        ),
        (
            Path('./tests/test_data/file1'),
            0,
            '1\n',
            1
        ),
        (
            Path('./tests/test_data/file5'),
            2,
            '5 file',
            3
        ),
        (
            Path('./tests/test_data/file5'),
            3,
            'my test\n',
            1
        )
    ]
)
def test_read_next_line(
    input_file_path,
    start_position,
    expected_line,
    expected_end_position
):
    result_line, result_end_position = read_next_line(
        input_file_path, start_position
    )
    assert result_line == expected_line
    assert result_end_position == expected_end_position


@pytest.mark.parametrize(
    'input_test_data,exception_type',
    [
        (
            Path('./test/test_data/file6'),
            FileNotFoundError
        ),
        (
            Path('./tests/test_data/directory'),
            IsADirectoryError
        )
    ]
)
def test_read_next_line_exceptions(input_test_data, exception_type):
    with pytest.raises(exception_type):
        read_next_line(
            input_test_data, 0
        )


@pytest.mark.parametrize(
    'input_test_data,expected_data',
    [
        (
            (
                './tests/test_data/file1',
                './tests/test_data/file2',
                './tests/test_data/file3'
            ),
            '1A-2B+3C-1D+2A-'
        ),
        (
            (
                './tests/test_data/file3',
                './tests/test_data/file5'
            ),
            '-my test+A-5 file+my test-A+5 file-my test+'
        )
    ]
)
def test_file_generator(input_test_data, expected_data):
    result_string = str()
    test_generator = file_generator(*input_test_data)
    for _ in range(15):
        result_string += next(test_generator)

    assert result_string == expected_data


@pytest.mark.parametrize(
    'input_test_data,expected_error',
    [
        (
            ('./tests/test_data/file5', None),
            {
                'expected_exception': TypeError,
                'match': 'File path must be str',
            }
        )
    ]
)
def test_file_generator_exceptions(input_test_data, expected_error):
    with pytest.raises(**expected_error):
        next(file_generator(*input_test_data))
