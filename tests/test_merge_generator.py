import pytest
from app.merge_generator import merge


@pytest.mark.parametrize(
    "input_test_data,expected_result",
    [
        pytest.param(
            (iter((0, 1, 6)), iter((1, 2, 3))),
            [0, 1, 1, 2, 3, 6],
            id='tz_test_case'
        ),
    ]
)
def test_merge_function(input_test_data, expected_result):
    output_data = [next_elem for next_elem in merge(*input_test_data)]
    assert output_data == expected_result


@pytest.mark.parametrize(
    'input_test_data,expected_error',
    [
        pytest.param(
            (iter((0, 1, 6)), iter((1, '2', 3))),
            {
                'expected_exception': TypeError,
                'match': 'Iterables must contain integer values'
            },
            id='str_type_element_in_input_iterator'
        ),
        pytest.param(
            (iter((0, 1, 6)), None),
            {
                'expected_exception': TypeError,
                'match': 'Some count input objects not iterable'
            },
            id='none_type_input_data'
        ),
        pytest.param(
            (iter((6, 1, 2)), iter((1, 2, 3))),
            {
                'expected_exception': ValueError,
                'match': 'Input iterators not sorted'
            },
            id='first_iterator_not_sorted'
        ),
        pytest.param(
            (iter((0, 1, 2)), iter((1, 2, 7, 2))),
            {
                'expected_exception': ValueError,
                'match': 'Input iterators not sorted'
            },
            id='second_iterator_not_sorted'
        )
    ]
)
def test_merge_function_exceptions(input_test_data, expected_error):
    with pytest.raises(**expected_error):
        result = [next_elem for next_elem in merge(*input_test_data)]
